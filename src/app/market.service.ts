import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json',
                              'X-Size': '50000' })
};

@Injectable({ providedIn: 'root' })
export class MarketService {

  private requestUrl = 'https://api.zonky.cz/loans/marketplace';

  constructor(
    private http: HttpClient) { }

  getRatings () {
    const url = `${this.requestUrl}?fields=rating`;
    return this.http.get<Object[]>(url, httpOptions)
      .pipe(
        catchError(this.handleError('getRatings', []))
      );
  }

  getAmounts (ratingId: string) {
    const url = `${this.requestUrl}?fields=amount&rating__eq=${ratingId}`;
    return this.http.get<[Object[]]>(url, httpOptions)
      .pipe(
        catchError(this.handleError('getAmounts', []))
      );
  }

  formatCurrency (amount: number) {
    return amount &&
           String(Math.round(amount)).replace(/(-?\d+)(\d{3})/g, '$1 $2')
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}

export class Rating {
    id: string;
    name: string;
    description: string;
}

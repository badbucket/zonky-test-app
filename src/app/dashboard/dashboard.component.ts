import { Component, OnInit } from '@angular/core';
import { MarketService } from '../market.service';
import { Rating } from '../rating';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {

  avgAmount: number;
  ratings: Rating[];
  selectedRating: Rating;

  constructor(private MarketService: MarketService) { }

  ngOnInit() {
    this.avgAmount = 0;
    this.ratings = [
      {
        id: 'AAAAA',
        name: 'A**',
        description: 'minimální riziko'
      },
      {
        id: 'AAAA',
        name: 'A*',
        description: 'velmi nízké riziko'
      },
      {
        id: 'AAA',
        name: 'A++',
        description: 'nízké riziko'
      },
      {
        id: 'AA',
        name: 'A+',
        description: 'nízké riziko'
      },
      {
        id: 'A',
        name: 'A',
        description: 'nižší riziko'
      },
      {
        id: 'B',
        name: 'B',
        description: 'střední riziko'
      },
      {
        id: 'C',
        name: 'C',
        description: 'vyšší riziko'
      },
      {
        id: 'D',
        name: 'D',
        description: 'rizikové'
      }
    ];
  }

  onSelectRating(rating: Rating) {
    this.avgAmount = null;
    this.selectedRating = rating;
    this.MarketService.getAmounts(rating.id)
      .subscribe(amounts => {
        this.avgAmount = (amounts as Object[])
          .map(amount => amount['amount'])
          .reduce((a, b) => a + b, 0)
          / amounts.length;
      });
  }

}

Cílem je vytvořit webovou aplikaci v Javascriptu, která spočítá průměrnou výši půjček (v API
pole amount) na tržišti Zonky pro uživatelem zvolený rating (např. uživatel vybere rating A a
aplikace zobrazí číslo 215000,-). Jedině kde můžete narazit je CORS. Zde doporučuji pro
dotazy použít proxy https://crossorigin.me/ nebo vypnout zabezpečení přímo v prohlížeči.

Reference:
https://app.zonky.cz
http://docs.zonky.apiary.io/#reference/marketplace/get
